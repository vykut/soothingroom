//
//  ShakeViewController.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 06/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import UIKit
import SpriteKit

class ShakeViewController: CustomViewController {
    
    @IBOutlet weak var onboardingContainer: UIView!
    
    override var tutorial: String! {
        return Constants.Onboard.ShakeTutorial
    }
    
    var isTutorial = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isTutorial = !UserDefaults.standard.bool(forKey: Constants.Onboard.ShakeTutorial)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        guard let view = view as? SKView else { return }
        view.isPaused = true
        guard let scene = view.scene as? GameScene else { return }
        scene.motionManager.stopDeviceMotionUpdates()
        scene.ballObserver.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let view = view as? SKView else { return }
        
        if isTutorial {
            onboardingContainer.isHidden = false
            view.isPaused = true
            return
        }
        
        guard let scene = view.scene as? GameScene else { return }
        scene.setMotionManager()
        scene.setBallObserver()
        
        if view.isPaused {
            view.isPaused = false
        }
    }
    
    func setScene() {
        guard let view = view as? SKView else { return }
        let scene = GameScene(size: view.bounds.size)
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        view.ignoresSiblingOrder = false
        view.backgroundColor = UIColor.white
        view.presentScene(scene)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isTutorial {
            guard let view = view as? SKView else { return }
            onboardingContainer.isHidden = true
            view.isPaused = false
            isTutorial = false
            setScene()
        }
    }
    
    override func viewAppeared() {
        guard let view = view as? SKView else { return }
        guard view.scene != nil else {
            setScene()
            return
        }
    }
}
