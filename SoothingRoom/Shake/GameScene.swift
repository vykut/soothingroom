

import SpriteKit
import CoreMotion
import UIKit
import Foundation
import AudioToolbox


class GameScene: CustomScene {
    
    let ballBitMask : UInt32 = 0x1 << 1
    let wallBitMask : UInt32 = 0x1 << 2
    
    var motionManager = CMMotionManager()
    //Istanciating background
    
    //Istanciating ball
    var balls = [SKSpriteNode]()
    
    var ballObserver: Timer!
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = .zero
        
        background.physicsBody?.categoryBitMask = wallBitMask
        background.physicsBody?.collisionBitMask = ballBitMask
        
        setBall(position: .zero)
        
        setMotionManager()
        
        setBallObserver()
        
        print(modelIdentifier())
    }
    
    func setBallObserver() {
        ballObserver = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { (timer) in
            self.balls.removeAll(where: { (ball) -> Bool in
                if !self.intersects(ball) {
                    ball.removeFromParent()
                    return true
                }
                return false
            })
        })
        ballObserver.fire()
    }
    
    func modelIdentifier() -> String {
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return valuet
        
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }
    
    func setBall(position: CGPoint) {
        let ball = SKSpriteNode(imageNamed: "ball")
        ball.position = position
        ball.size = CGSize(width: size.width * 0.1, height: size.width * 0.1)
        
        ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.size.width / 2 + 1)
        ball.physicsBody?.categoryBitMask = ballBitMask
        ball.physicsBody?.contactTestBitMask = ballBitMask | wallBitMask
        ball.physicsBody?.collisionBitMask = wallBitMask | ballBitMask
        ball.physicsBody?.usesPreciseCollisionDetection = true
        ball.physicsBody?.restitution = 0.5
        ball.physicsBody?.allowsRotation = false
        
        balls.append(ball)
        background.addChild(ball)
    }
    
    func setMotionManager() {
        motionManager.deviceMotionUpdateInterval = 1 / 30
        
        let accelerationMultiplier = 50.0
        let gravityMultiplier = 10.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.motionManager.startDeviceMotionUpdates(to: OperationQueue()) { (motion, err) in
                guard err == nil else { return }
                guard let motion = motion else { return }
                
                print("userAcceleration X: \(motion.userAcceleration.x)")
                print("userAcceleration Y: \(motion.userAcceleration.y)")
                
                self.balls.forEach({ (ball) in
                    
                    ball.physicsBody?.applyImpulse(CGVector(dx: motion.userAcceleration.x * accelerationMultiplier + motion.gravity.x * gravityMultiplier, dy: motion.userAcceleration.y * accelerationMultiplier + motion.gravity.y * gravityMultiplier))
                })
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //        guard let touch = touches.first else { return }
        //        let touchLocation = touch.location(in: self)
        let touchLocations = touches.compactMap({$0.location(in: self)})
        
        touchLocations.forEach { (touchLocation) in
            
            let nodes = background.nodes(at: touchLocation)
            
            if !nodes.isEmpty {
                balls.removeAll { (ball) -> Bool in
                    if nodes.contains(ball) {
                        ball.removeFromParent()
                        return true
                    }
                    return false
                }
            } else {
                setBall(position: touchLocation)
            }
        }
        
        print("Number of balls \(balls.count)")
    }
}

extension GameScene: SKPhysicsContactDelegate {
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if contact.bodyA.categoryBitMask == contact.bodyB.collisionBitMask || contact.bodyB.categoryBitMask == contact.bodyA.collisionBitMask {
            VibrationManager.vibrator.vibrate(intensity: 5)
            return
        }
        
        if contact.bodyA.categoryBitMask == contact.bodyB.categoryBitMask {
            VibrationManager.vibrator.vibrate(intensity: 4)
        }
    }
}
