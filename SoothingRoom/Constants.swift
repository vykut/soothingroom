//
//  Constants.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 06/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    
    static let Colors = [Constants.Onboard.color1, Constants.Onboard.color2, Constants.Onboard.color3, Constants.Onboard.color4]
    static let Locations = [0, 0.25, 0.5, 1]
    
    struct Storyboards {
        static let MainStoryboard = "Main"
        static let ShakeStoryboard = "ShakeStoryboard"
        static let TouchStoryboard = "TouchStoryboard"
        static let ScratchStoryboard = "ScratchStoryboard"
        static let SoundsStoryboard = "SoundsStoryboard"
    }
    
    struct Onboard {
        static let ShakeTutorial = "ShakeTutorial"
        static let SoundsTutorial = "SoundsTutorial"
        static let TouchTutorial = "TouchTutorial"
        static let ScratchTutorial = "ScratchTutorial"
        
        static let ViewController = "ModalViewController"
        
        //light
        static let color1 = UIColor.UIColorFromRGB(rgbValue: 0x0351B8)
        static let color2 = UIColor.UIColorFromRGB(rgbValue: 0x3E91D5)
        static let color3 = UIColor.UIColorFromRGB(rgbValue: 0x47D3EF)
        static let color4 = UIColor.UIColorFromRGB(rgbValue: 0xC0F0F9)
        //dark
        
        var imageName: String
        var title: String
        var description: String
        
        static let ShakeOnboard = Onboard(imageName: "shake", title: NSLocalizedString("tutorial_shake_title", comment: ""), description: NSLocalizedString("tutorial_shake_description", comment: ""))
        static let SoundsOnboard = Onboard(imageName: "sounds", title: NSLocalizedString("tutorial_sounds_title", comment: ""), description: NSLocalizedString("tutorial_sounds_description", comment: ""))
        static let TouchOnboard = Onboard(imageName: "touch", title: NSLocalizedString("tutorial_touch_title", comment: ""), description: NSLocalizedString("tutorial_touch_description", comment: ""))
        
        static func getOnboard(sender: String) -> Onboard? {
            switch sender {
            case ShakeTutorial:
                return ShakeOnboard
            case SoundsTutorial:
                return SoundsOnboard
            case TouchTutorial:
                return TouchOnboard
            case ScratchTutorial:
                //to be added scratch onboard
                fallthrough
            default:
                return nil
            }
        }
    }
}
