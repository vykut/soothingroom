//
//  TouchViewController.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 06/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import UIKit
import SpriteKit

class TouchViewController: CustomViewController {

    var height: CGFloat = 0
    var width: CGFloat = 0
    var timer: Timer?
    var range : Int = 0
    
    var generator: UIFeedbackGenerator!
    var feedbackType: UINotificationFeedbackGenerator.FeedbackType?
        
    @objc func vibrate() {
        VibrationManager.vibrator.vibrate(intensity: range)
        }
    
    override var tutorial: String! {
        return Constants.Onboard.TouchTutorial
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        if let view = view as? SKView {
            let scene = TouchScene(size: view.bounds.size)
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            view.ignoresSiblingOrder = false
            view.backgroundColor = UIColor.white
            view.presentScene(scene)
        }
        
        height = view.frame.maxY / 6
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        guard timer != nil else { return }
        timer!.invalidate()
    }
    
    fileprivate func setVibration(_ point: CGPoint) {
        let number = point.y
        range = Int(number / height)
        
        print(range)
        
        if timer != nil {
            timer!.invalidate()
        }
        
        let timeInterval: Double = Double((view.frame.maxX - point.x) / view.frame.maxX) + 0.05
        
        timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(vibrate), userInfo: nil, repeats: true)
        
        timer!.fire()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self.view) else { return }
        setVibration(point)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesBegan(touches, with: event)
    }
}
