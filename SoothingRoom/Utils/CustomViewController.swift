//
//  CustomViewController.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 07/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import UIKit

class CustomViewController: UIViewController {
    
    var tutorial: String! {
        return ""
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // leave this function final
    // if you want to call viewDidAppear in subclass, override viewAppeared
    override final func viewDidAppear(_ animated: Bool) {
        showModal()
        viewAppeared() // override it in subclass
    }
    
    func showModal() {
        let presented = UserDefaults.standard.bool(forKey: tutorial)
        if !presented {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: Constants.Onboard.ViewController) as? ModalViewController, let onboard = Constants.Onboard.getOnboard(sender: tutorial) else { return }
            vc.tutorial = tutorial
            vc.onboard = onboard
            guard let tabBar = tabBarController else { print("Trying to present tutorial but tabBar is nil"); return }
            tabBar.present(vc, animated: true, completion: nil)
        }
    }
    
    //override it in superclass; leave implementation empty in this class
    func viewAppeared() {
        
    }
    

}
