//
//  CustomScene.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 11/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import UIKit
import SpriteKit

class CustomScene: SKScene {
    
    var background: SKSpriteNode!

    override func didMove(to view: SKView) {
        
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        setSceneBackground()
    }
    
    func setSceneBackground() {
        
        let backgroundTexture = SKTexture(image: UIImage.gradientImageWithBounds(bounds: frame, colors: Constants.Colors.map({$0.cgColor}), locations: [0, 0.25, 0.5, 1].map({$0 as NSNumber})))
        background = SKSpriteNode(texture: backgroundTexture)
        
        background.size = size
        background.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        background.color = .white
        
        print("bottom area insets: \(view!.safeAreaInsets.bottom)\ntop area insets: \(view!.safeAreaInsets.top)")
        
        background.physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(x: frame.minX, y: frame.minY + view!.safeAreaInsets.bottom, width: frame.width, height: frame.height - view!.safeAreaInsets.bottom - view!.safeAreaInsets.top))
        background.physicsBody?.usesPreciseCollisionDetection = true
        background.physicsBody?.restitution = 0.5
        background.physicsBody?.isDynamic = false
        addChild(background)
    }
    
}
