//
//  Extensions.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 06/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

extension UIColor {
    class func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIViewController {
    func setBackground() {
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = Constants.Colors.map({ $0.cgColor })
        gradient.locations = Constants.Locations.map({ $0 as NSNumber })
        view.layer.insertSublayer(gradient, at: 0)
    }
}

extension UIImage {
    static func gradientImageWithBounds(bounds: CGRect, colors: [CGColor], locations: [NSNumber]) -> UIImage {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = colors
        gradientLayer.locations = locations
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
