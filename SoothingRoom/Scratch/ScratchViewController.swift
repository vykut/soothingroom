//
//  ScratchViewController.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 06/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import UIKit
import AVFoundation

class ScratchViewController: CustomViewController {
    
    var soundDataAsset: NSDataAsset!
    
    var i = 0
    
    @IBOutlet weak var materialImageView: UIImageView!
    
    override var tutorial: String! {
        return Constants.Onboard.ScratchTutorial
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Scratch", comment: "Title of the scratch in tabbar")

        
        
        soundDataAsset = NSDataAsset(name: "carpetSound4")!
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
}
