//
//  SoundsController.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 08/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer

class SoundsController: NSObject {
    
    private var player: AVAudioPlayer?
    
    // used for both the title and the image
    private(set) var songString = ""
    
    weak var delegate: AVAudioPlayerDelegate? {
        set {
            player?.delegate = newValue
        }
        get {
            return player?.delegate
        }
    }
    
    var isPlaying: Bool {
        print(player?.isPlaying ?? "player does not exist")
        return player?.isPlaying ?? false
    }
    
    static let audioPlayer = SoundsController()
    
    private override init() {
        super.init()
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, policy: .default, options: [.allowBluetooth])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error {
            print("error: \(error)")
        }
        
    }
    
    func play(songTitle: String, songData: Data) {
        if let player = player, player.isPlaying {
            player.stop()
        }
        
        do {
            self.player = try AVAudioPlayer(data: songData)
            songString = songTitle
            print(songString)
            player!.play()
            
            MPNowPlayingInfoCenter.default().playbackState = .playing
            
            setupCommandCenter()
            
        } catch let error {
            print(error)
        }
    }
    
    @objc func pause() {
        if !isPlaying {
            return
        }
        
        MPNowPlayingInfoCenter.default().playbackState = .paused
        
        player?.pause()
    }
    
    @objc func resume() {
        if isPlaying {
            return
        }
        
        MPNowPlayingInfoCenter.default().playbackState = .playing
        
        player?.play()
    }
    
    func restart() {
        guard let player = player else { return }
        
        MPNowPlayingInfoCenter.default().playbackState = .playing
        
        player.stop()
        player.currentTime = 0
        player.play()
    }
    
    func stop() {
        
        MPNowPlayingInfoCenter.default().playbackState = .paused
        
        player?.stop()
        player?.currentTime = 0
    }
    
    func setupCommandCenter() {
        let artwork = MPMediaItemArtwork(boundsSize: CGSize(width: 40, height: 40)) { (size) -> UIImage in
            return UIImage(imageLiteralResourceName: self.songString)
        }
        
        let commandCenter = MPRemoteCommandCenter.shared()
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: NSLocalizedString(songString, comment: ""), MPMediaItemPropertyArtwork: artwork]
        
        commandCenter.playCommand.isEnabled = true
        commandCenter.pauseCommand.isEnabled = true
        
        commandCenter.playCommand.addTarget(self, action: #selector(resume))
        commandCenter.pauseCommand.addTarget(self, action: #selector(pause))
    }
}
