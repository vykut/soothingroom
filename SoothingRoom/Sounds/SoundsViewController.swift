
//
//  SoundViewController.swift
//  BUProject
//
//  Created by Fabrizio Di Rosa on 04/02/2019.
//  Copyright © 2019 Black Unicorn. All rights reserved.
//

import UIKit
import AVFoundation

class SoundsViewController: CustomViewController {
    
    //   Variables declaration
    var player = SoundsController.audioPlayer
    
    var hasBeenPaused : (isPaused : Bool, rowPaused : Int) = (false, -1)
    var isPlaying : (isPlaying : Bool, rowPlaying : Int) = (false, -1)
    
    let dataModel = [NSLocalizedString("RainAndBirds", comment: "RainAndBirds sound"),
                    NSLocalizedString("OceanWaves", comment: "OceanWaves sound"),
                    NSLocalizedString("Thunderstorm", comment: "Thunderstorm sound"),
                    NSLocalizedString("Waterfall", comment: "Waterfall sound"),
                    NSLocalizedString("Rain", comment: "Rain sound"),
                    NSLocalizedString("Waves", comment: "Waves sound"),
                    NSLocalizedString("Thunder", comment: "Thunder sound"),
                    NSLocalizedString("River", comment: "River sound"),
                    NSLocalizedString("Ocean", comment: "Ocean sound"),
                    NSLocalizedString("SeaStorm", comment: "SeaStorm sound")]
    
    let songModel = ["RainAndBirds", "OceanWaves", "Thunderstorm", "Waterfall", "Rain", "Waves", "Thunder", "River", "Ocean", "SeaStorm"]
    
    //  Declaration on toolbar buttons
    var playBarButton = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(resume))
    
    var pauseBarButton = UIBarButtonItem(barButtonSystemItem: .pause, target: self, action: #selector(pause))
    
    let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    
    let songInfoBarButton = UIBarButtonItem(title: "Song", style: .plain, target: self, action: nil)
    
    let restartBarButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(restart))
    
    let background = UIImageView()
    
    //  Outlets declaration
    @IBOutlet weak var soundTableView: UITableView!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var toolbarBottomConstraint: NSLayoutConstraint!
    
    override var tutorial: String! {
        return Constants.Onboard.SoundsTutorial
    }
    
    private(set) var pauseToolbar = [UIBarButtonItem]()
    private(set) var playToolbar = [UIBarButtonItem]()
    
    @objc func resume() {
        print("Play Tapped")
        self.player.resume()
        // Replace Play with Pause
        self.toolbar.items = pauseToolbar
        pauseBarButton.isEnabled = true
    }
    
    @objc func pause() {
        print("Pause Tapped")
        self.player.pause()
        // Replacing pause with play
        self.toolbar.items = playToolbar
        playBarButton.isEnabled = true
    }
    
    @objc func restart() {
        print("Restart Tapped")
        player.restart()
        
        if let item = toolbar.items?.first, item == playBarButton {
            toolbar.items = pauseToolbar
        }
    }

    override func viewDidLoad() {
         super.viewDidLoad()
        
        
        self.view.backgroundColor = .white
        
        self.soundTableView.delegate = self
        self.soundTableView.dataSource = self
        
        pauseToolbar = [restartBarButton, spacer, songInfoBarButton, spacer, pauseBarButton]
        playToolbar = [restartBarButton, spacer, songInfoBarButton, spacer, playBarButton]
        
        toolbar.items = pauseToolbar
        
        NotificationCenter.default.addObserver(self, selector: #selector(pauseBackground), name: nil, object: nil)
    }
    
    @objc func pauseBackground() {
        DispatchQueue.main.async {
            guard UIApplication.shared.applicationState == .background || UIApplication.shared.applicationState == .inactive else { return }
            
            if self.player.isPlaying {
                self.toolbar.items = self.pauseToolbar
            } else {
                self.toolbar.items = self.playToolbar
            }
        }
    }
    
    func presentAlertSongNotFoundAndShowPlayButton() {
        let alert = UIAlertController(title: "Error", message: "Sound not found", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert,animated: true)
        self.toolbar.items = playToolbar
        self.songInfoBarButton.title = ""
        
        toolbarBottomConstraint.constant = 100
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - TableView
extension SoundsViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // Declaring the cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell", for: indexPath) as? SoundTableViewCell

        // Putting image in the UIimageview of the cell,the default image for nil is "NotFound"
        if let image = UIImage(named: songModel[indexPath.row]) {
            cell?.myImageView.image = image
        } else {
            cell?.myImageView.image = UIImage(named: "notFound")
        }

        // Putting other things
        cell?.titleLabel.text = dataModel[indexPath.row]

        return cell ?? UITableViewCell()

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.soundTableView.deselectRow(at: indexPath, animated: true)

        if toolbarBottomConstraint.constant != 0 {
            toolbarBottomConstraint.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }

        guard let songData = NSDataAsset(name: "\(songModel[indexPath.row])Sound") else { presentAlertSongNotFoundAndShowPlayButton(); return }

        songInfoBarButton.title = dataModel[indexPath.row]

        toolbar.items = pauseToolbar

        player.delegate = self
        print(songData.data)
        player.play(songTitle: songModel[indexPath.row], songData: songData.data)
    }
}

extension SoundsViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.toolbarBottomConstraint.constant = 100
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
