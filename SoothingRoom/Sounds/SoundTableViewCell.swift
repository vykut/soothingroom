//
//  SoundTableViewCell.swift
//  BUProject
//
//  Created by Fabrizio Di Rosa on 04/02/2019.
//  Copyright © 2019 Black Unicorn. All rights reserved.
//

import UIKit

class SoundTableViewCell: UITableViewCell {

    @IBOutlet weak var playButton: UIButton!
    @IBOutlet var myImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel! = {
        var label = UILabel()
        label.text = "description"
        return label
    }()
    @IBOutlet weak var titleLabel: UILabel! = {
        var label = UILabel()
        label.text = "description"
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
