//
//  VibrationManager.swift
//  SoothingRoom
//
//  Created by Fabrizio Di Rosa on 11/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class VibrationManager {
    
    static let vibrator =  VibrationManager()
    
    let isVibrationAvailable = UIDevice.current.value(forKey: "_feedbackSupportLevel") as? Int ?? -1

    private init() {}
    
    func vibrate(intensity: Int) {
        
        guard isVibrationAvailable == 2 else {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            return
        }
        
        switch intensity {
        case 0:
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
        case 1:
            UINotificationFeedbackGenerator().notificationOccurred(.warning)
        case 2:
            UINotificationFeedbackGenerator().notificationOccurred(.error)
        case 3:
            UINotificationFeedbackGenerator().notificationOccurred(.success)
        case 4:
            UIImpactFeedbackGenerator(style: .medium).impactOccurred()
        case 5:
            UIImpactFeedbackGenerator(style: .heavy).impactOccurred()
        default:
            return
        }
    }
}
