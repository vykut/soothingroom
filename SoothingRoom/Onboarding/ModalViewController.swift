//
//  ModalSoundsViewController.swift
//  SoothingRoom
//
//  Created by Victor Socaciu on 07/02/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController {

    @IBOutlet weak var modalImage: UIImageView!
    @IBOutlet weak var modalTitle: UILabel!
    @IBOutlet weak var modalDescription: UILabel!
    
    var onboard: Constants.Onboard!
    var tutorial: String!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        modalImage.image = UIImage(imageLiteralResourceName: onboard.imageName)
        modalTitle.text = onboard.title
        modalDescription.text = onboard.description
    }
    
    override func viewDidLayoutSubviews() {
        setBackground()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UserDefaults.standard.set(true, forKey: tutorial)
        dismiss(animated: true, completion: nil)
    }
}
